from peewee import *

PAIR_DB_PATH = 'pair.db'

pair_db = SqliteDatabase(":memory:")

class Group(Model):
    name = CharField(unique=True)

    class Meta:
        database = pair_db

class Prepod(Model):
    name = CharField(unique=True)

    class Meta:
        database = pair_db

class Week(Model):
    number = IntegerField(unique=True)

    class Meta:
        database = pair_db

class Pair(Model):
    group = ForeignKeyField(Group, backref='pairs')
    day = IntegerField()
    number = IntegerField()
    type = CharField()
    subgroup = IntegerField()
    name = CharField()
    place = CharField()
    lector = ForeignKeyField(Prepod, backref='pairs')

    class Meta:
        database = pair_db

class WeekPair(Model):
    pair = ForeignKeyField(Pair, backref='weeks')
    week = ForeignKeyField(Week, backref='pairs')

    class Meta:
        database = pair_db

def init_pair_db():
    pair_db.create_tables([Group, Week, Pair, WeekPair, Prepod])
    for i in range(1,25):
        Week(number=i).save()

def clear_pair_db():
    import os
    try:
        os.remove(PAIR_DB_PATH)
    except Exception:
        pass

def move_to_file(path = PAIR_DB_PATH):
    import sqlite3
    from io import StringIO

    tempfile = StringIO()
    for line in pair_db.connection().iterdump():
        tempfile.write('%s\n' % line)
    tempfile.seek(0)

    clear_pair_db()

    # Create a database in memory and import from tempfile
    sqlite = sqlite3.connect(path)
    sqlite.cursor().executescript(tempfile.read())
    sqlite.commit()
    sqlite.close()

def move_pair_db(new_path):
    global pair_db, PAIR_DB_PATH
    import shutil
    try:
        shutil.move(PAIR_DB_PATH, new_path)
        PAIR_DB_PATH = new_path
        pair_db = SqliteDatabase(PAIR_DB_PATH)
    except Exception:
        pass




