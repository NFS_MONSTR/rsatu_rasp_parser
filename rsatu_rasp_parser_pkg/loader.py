def download_file(url, filename, use_proxy=False, proxy_addr='', proxy_type='http'):
    from urllib import request as req
    import shutil
    r = req.Request(url)
    if use_proxy:
        r.set_proxy(proxy_addr, proxy_type)
    with req.urlopen(r, timeout=10) as response, open(filename, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)


def load_file(filename, is_xlsx):
    import pyexcel_xls
    import pyexcel_xlsx
    if is_xlsx:
        return pyexcel_xlsx.get_data(filename, auto_detect_float=False)
    else:
        return pyexcel_xls.get_data(filename)


# def get_prepared_data():
#     #import os
#     URL = 'http://www.rsatu.ru/arch/rasp/raspisanie.xls'
#     TEMP_FILENAME = 'rasp.xls'
#     try:
#         download_file(URL, TEMP_FILENAME)
#     except Exception as e:
#         print(e)
#     result = load_file(TEMP_FILENAME)
#     #os.remove(TEMP_FILENAME)
#     return result[list(result.keys())[0]]

def get_prepared_data(use_proxy=False, proxy_addr='', proxy_type='http'):
    from urllib import request as req
    from html.parser import HTMLParser
    TEMP_FILENAME = 'rasp.xls'
    is_xlsx = False
    try:
        r = req.Request('https://www.rsatu.ru/persons/schedule/')
        if use_proxy:
            r.set_proxy(proxy_addr, proxy_type)
        resp = req.urlopen(r, timeout=10)
        text = str(resp.read())

        URL = ''
        max_number = ''

        class RHTMLParser(HTMLParser):
            def handle_starttag(self, tag, attrs):
                nonlocal URL, max_number, is_xlsx
                if tag == 'a':
                    is_xls = False
                    number = ''
                    url = ''
                    for attr in attrs:
                        if attr[0] == 'href' and 'xls' in attr[1]:
                            is_xls = True
                            url = ('' if 'rsatu.ru' in attr[1] else 'http://www.rsatu.ru') + attr[1]
                            print(url)
                            number = attr[1][15:]
                            number = number[:number.index('/')]
                    if is_xls and number > max_number and not URL:
                        URL = url
                        max_number = number
                        is_xlsx = 'xlsx' in url

        RHTMLParser().feed(text)
        try:
            if is_xlsx:
                TEMP_FILENAME = 'rasp.xlsx'
            download_file(URL, TEMP_FILENAME, use_proxy, proxy_addr, proxy_type)
        except Exception as e:
            print(e)
    except Exception as e:
        print('Не получилось скачать')
        print(e)
    result = load_file(TEMP_FILENAME, is_xlsx)
    # os.remove(TEMP_FILENAME)
    offset = 0
    if 'изменения' in list(result.keys())[0]:
        offset = 1
    return [result[list(result.keys())[offset]], result[list(result.keys())[offset+1]], result[list(result.keys())[offset+2]]]


def load_rasp_date(use_proxy=False, proxy_addr='', proxy_type='http'):
    from urllib import request as req
    from html.parser import HTMLParser
    try:
        r = req.Request('https://www.rsatu.ru/students/raspisanie-zanyatiy/')
        if use_proxy:
            r.set_proxy(proxy_addr, proxy_type)
        resp = req.urlopen(r, timeout=10)
        text = str(resp.read().decode(resp.headers.get_content_charset()))
        edit_pos = text.find('Последнее редактирование')
        if edit_pos < 0:
            return None
        end_pos = text.find('<', edit_pos)
        if end_pos < 0:
            end_pos = len(text) - 1
        return text[edit_pos:end_pos]

    except Exception as e:
        print('Не удалось загрузить дату')
        print(e)
        return None
