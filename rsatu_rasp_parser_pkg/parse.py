import datetime
import re

from rsatu_rasp_parser_pkg.db import init_pair_db, clear_pair_db, move_pair_db, Pair, WeekPair, Week, pair_db, Group, \
    move_to_file, Prepod
from rsatu_rasp_parser_pkg.loader import get_prepared_data, load_rasp_date

week_days = {
    'пн': 0,
    'вт': 1,
    'ср': 2,
    'чт': 3,
    'пт': 4,
    'сб': 5,
    'вс': 6
}

week_days_reversed = {
    0: 'пн',
    1: 'вт',
    2: 'ср',
    3: 'чт',
    4: 'пт',
    5: 'сб',
    6: 'вс'
}

db_weeks = []
sem_begin = datetime.date(year=2023, month=2, day=6)


def strip_to_dig(s):
    while not (s[-1:] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']):
        s = s[:-1]
    return s


def strip_to_digl(s):
    while not (s[:1] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']):
        s = s[1:]
    return s


SUBGR_REGEX = re.compile(r'[а-яА-Яa-zA-Z.]{2,9}\d?\d?-\d\d[а-яА-Яa-zA-Z]?')
PAIR_IN_DAY = 7
DAYS_IN_WEEK = 6

all_groups = []
all_prepods = []
prepod_names = []


def isCurrentGroup(group, value):
    return (value == group or value == (group + '-1') or value == (group + '-2') or value == (
            group + '-3') or value == (group + '-4'))


def isGroup(value):
    for group in all_groups:
        if isCurrentGroup(group, value):
            return True
    return False


def isPrepodName(value, value2):
    for idx, name in enumerate(prepod_names):
        if name['name'] == value and name['initials'] == value2:
            return idx
    for idx, name in enumerate(prepod_names):
        if name['name'] == value:
            return idx
    return -1


def findPrepod(components):
    prepodIdx = -1
    name_idx = -1
    for idx, c in enumerate(components):
        name_idx = isPrepodName(c, components[idx + 1] if idx + 1 < len(components) else '')
        if name_idx > -1:
            prepodIdx = idx
            break
    if prepodIdx <= -1:
        return []
    if components[prepodIdx + 1] == prepod_names[name_idx]['initials']:
        return [prepodIdx, prepodIdx + 1]
    else:
        return [prepodIdx]


def findWeekInfo(components):
    weekInfoBegin = -1
    weekInfoEnd = -1
    for idx, c in enumerate(components):
        if c.startswith('(Н'):
            weekInfoBegin = idx
            continue
        if c.endswith(')') and weekInfoBegin > -1:
            weekInfoEnd = idx
            break
    if (weekInfoBegin > -1 and weekInfoEnd > -1):
        return [weekInfoBegin, weekInfoEnd]
    else:
        return []


def isType(value):
    return value in ['Л', 'ЛР', 'П', 'оЛ', 'оЛР', 'оП']

def filter_odd_num(in_num):
    if(in_num % 2) == 0:
        return True
    else:
        return False

def filter_not_odd_num(in_num):
    if(in_num % 2) != 0:
        return True
    else:
        return False

def getWeeks(line, isOdd):
    weeks = []
    if '-' in line:
        str_begin, str_end = line.split('-')
        addWeeks = []
        if ',' in str_end:
            splitted = str_end.split(',')
            str_end = splitted[0]
            addWeeks = getWeeks(','.join(splitted[1:]), isOdd)
        begin,end = map(int,[str_begin, str_end])
        weeks = [x for x in range(begin, end+1)] + addWeeks
    elif ',' in line:
        weeks = map(int,line.split(','))
    return list(filter(filter_odd_num, weeks) if isOdd else filter(filter_not_odd_num, weeks))

def parsePairLine(pair, group, subgr, pairNumber, dayOfWeek, isOdd):
    if len(pair) == 0:
        return
    pair = pair.replace(' Печатки ', ' Печаткин ')
    pair = pair.replace(' Печаткин Г', ' Печаткин А.В. Г')

    # print(pair, subgr, pairNumber + 1, week_days_reversed[dayOfWeek], isOdd)
    components = pair.split(' ')
    for i in range(0, len(components)):
        if isCurrentGroup(group, components[i]):
            components = components[i:]
            break
    if not isCurrentGroup(group, components[0]):
        return
    while isGroup(components[1]):
        components = components[0:1] + components[2:]
    prepodPositions = findPrepod(components)
    prepod = ' '.join(components[x] for x in prepodPositions)
    weekPositions = findWeekInfo(components)
    nameBegin = 1
    nameEnd = min(min(prepodPositions + [1000]), min(weekPositions + [1000]))
    nameEnd -= 1
    name = ' '.join(components[x] for x in range(nameBegin, nameEnd))
    typePosition = nameEnd
    type = components[typePosition]
    place = components[-1]
    if (place == 'спортзал' or place == 'кипения'):
        place = components[-2] + ' ' + components[-1]
    weeksLine = ''
    if len(weekPositions) > 1:
        weeksLine = components[weekPositions[-1]].replace(')','')

    group, created = Group.get_or_create(name=group)
    lector, created = Prepod.get_or_create(name=prepod.strip())
    pairDb = Pair(group=group, day=dayOfWeek, number=pairNumber + 1, type=type, subgroup=subgr, name=name, place=place,
                lector=lector)
    pairDb.save()
    weeks = [x for x in range(1, 18, 2)] if isOdd else [x for x in range(2, 19, 2)]
    if weeksLine and len(weeksLine)>0:
        weeks = getWeeks(weeksLine, isOdd)
    for week in weeks:
        WeekPair.create(pair=pairDb, week=db_weeks[week - 1])


def getSubgr(group):
    try:
        return int(SUBGR_REGEX.sub('', group).replace('-', ''))
    except Exception:
        return 0


def removeGroupsFromPair(pair):
    for group in all_groups:
        pair = pair.replace(group + '-1', '')
        pair = pair.replace(group + '-2', '')
        pair = pair.replace(group + '-3', '')
        pair = pair.replace(group + '-4', '')
        pair = pair.replace(group, '')
    return pair.strip()


def parse_group(line):
    group = re.search(SUBGR_REGEX, line[0]).group(0)
    subgr = getSubgr(line[0])
    for idx, pair in enumerate(line[1:]):
        if (pair == 'СПДк-24 Инженерная и компьютерная графика ЛР (Недели 1-9) // Базовый курс NX (Недели 10-18) '
                    'Асекритова С.В. 2-205'):
            pair1 = 'СПДк-24 Базовый курс NX ЛР (Недели 10-18) Асекритова С.В. 2-205'
            pair = 'СПДк-24 Инженерная и компьютерная графика ЛР (Недели 1-9) Асекритова С.В. 2-205'
            parsePairLine(pair1, group, subgr, idx % PAIR_IN_DAY, (idx // PAIR_IN_DAY) % DAYS_IN_WEEK,
                          ((idx // PAIR_IN_DAY) // DAYS_IN_WEEK) == 0)
        if pair == 'СПДк-24 (Недели 1-6) СБ-24 (Недели 13-18) Блок Системы искусственного интеллекта ЛР Копачев М.Ю. Г-512' and group == 'СПДк-24':
            pair = 'СПДк-24 Блок Системы искусственного интеллекта ЛР (Недели 1-6) Копачев М.Ю. Г-512'
        if pair == 'СПДк-24 (Недели 1-6) СБ-24 (Недели 13-18) Блок Системы искусственного интеллекта ЛР Копачев М.Ю. Г-512' and group == 'СБ-24':
            pair = 'СБ-24 Блок Системы искусственного интеллекта ЛР (Недели 13-18) Копачев М.Ю. Г-512'
        parsePairLine(pair, group, subgr, idx % PAIR_IN_DAY, (idx // PAIR_IN_DAY) % DAYS_IN_WEEK,
                      ((idx // PAIR_IN_DAY) // DAYS_IN_WEEK) == 0)


def parse(use_proxy=False, proxy_addr='', proxy_type='http', _sem_begin=datetime.date(year=2023, month=2, day=6)):
    global db_weeks
    global sem_begin
    global all_groups
    global all_prepods
    global prepod_names
    sem_begin = _sem_begin
    try:
        pair_db.connect()
        init_pair_db()
        data = get_prepared_data(use_proxy, proxy_addr, proxy_type)
        rasp_sheet = data[0]
        prepod_sheet = data[1]
        all_prepods = prepod_sheet[0][3:]
        prepod_names = [{'name': x.split(' ')[0], 'initials': ' '.join(x.split(' ')[1:])} for x in all_prepods]
        db_weeks = Week.select().order_by(Week.number)
        all_groups = rasp_sheet[0][3:]
        rotated_data = list(reversed([[(str(rasp_sheet[j][i]).strip().replace('\n', '').replace('  ', ' ') if i < len(
            rasp_sheet[j]) else '') for j in range(len(rasp_sheet))]
                                      for i in
                                      range(len(rasp_sheet[0]) - 1, -1, -1)]))
        cleaned_data = list(filter(lambda line: len(str(line[0]).strip().replace(' ', '')) != 0, rotated_data[3:]))
        for idx, line in enumerate(cleaned_data):
            next_line = cleaned_data[idx + 1] if idx + 1 < len(cleaned_data) else ['']
            # Пропускаем объединенные группы
            if (line[0] in next_line[0]):
                continue
            parse_group(line)

    except Exception as e:
        pair_db.close()
        raise Exception(e)
    else:
        move_to_file()
        pair_db.close()


def get_last_rasp_date(use_proxy=False, proxy_addr='', proxy_type='http'):
    text = load_rasp_date(use_proxy, proxy_addr, proxy_type)
    dateformat = '%d.%m.%Y %H:%M'
    if text is None:
        return datetime.datetime(datetime.MAXYEAR, 1, 1, 1, 1, 1)
    text = strip_to_dig(text)
    text = strip_to_digl(text)
    try:
        dt = datetime.datetime.strptime(text, dateformat)
        return dt
    except Exception as e:
        return datetime.datetime(datetime.MAXYEAR, 1, 1, 1, 1, 1)


if __name__ == '__main__':
    parse()
