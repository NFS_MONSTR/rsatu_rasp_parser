import setuptools

#with open("README.md", "r") as fh:
#    long_description = fh.read()

setuptools.setup(
    name="rsatu-rasp-parser",
    version="0.0.1",
    author="NFS_MONSTR",
    author_email="nfsmonstr@gmail.com",
    description="",
    long_description='',
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
    ],
    python_requires='>=3.4',
)
